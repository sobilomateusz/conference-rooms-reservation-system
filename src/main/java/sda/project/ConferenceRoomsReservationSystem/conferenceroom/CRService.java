package sda.project.ConferenceRoomsReservationSystem.conferenceroom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.project.ConferenceRoomsReservationSystem.organization.OrgRepository;
import sda.project.ConferenceRoomsReservationSystem.organization.Organization;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class CRService {

    private final CRRepository crRepository;
    private final OrgRepository orgRepository;
    private final ConferenceRoomEntityTransformer conferenceRoomEntityTransformer;

    @Autowired
    public CRService(CRRepository crRepository,
                     OrgRepository orgRepository,
                     ConferenceRoomEntityTransformer conferenceRoomEntityTransformer) {
        this.crRepository = crRepository;
        this.orgRepository = orgRepository;
        this.conferenceRoomEntityTransformer = conferenceRoomEntityTransformer;
    }

    public ConferenceRoomEntity addConferenceRoom(ConferenceRoomEntity conferenceRoomEntity) {
        ConferenceRoom conferenceRoom = conferenceRoomEntityTransformer.fromEntity(conferenceRoomEntity);
        crRepository.findByNameAndOrganization_Name(conferenceRoom.getName(), conferenceRoom.getOrganization().getName())
                .ifPresent(cr -> {
                    throw new AlreadyExistingConferenceRoomException(conferenceRoom.getName());
                });
        Organization organization = orgRepository.findById(conferenceRoom.getOrganization().getName())
                .orElseThrow(() -> {
                    throw new NoSuchElementException();
                });
        conferenceRoom.setOrganization(organization);
        return conferenceRoomEntityTransformer.toEntity(crRepository.save(conferenceRoom));
    }

    public List<ConferenceRoomEntity> getAll() {
        return crRepository.findAll().stream()
                .map(conferenceRoomEntityTransformer::toEntity)
                .collect(Collectors.toList());
    }

    public ConferenceRoomEntity getById(String id) {
        return crRepository.findById(id)
                .map(conferenceRoomEntityTransformer::toEntity)
                .orElseThrow(() -> {
                    throw new NoSuchElementException();
                });
    }

    public ConferenceRoomEntity deleteConferenceRoom(String id) {
        ConferenceRoom conferenceRoom = crRepository.findById(id)
                .orElseThrow(() -> {
                    throw new NoSuchElementException();
                });
        crRepository.delete(conferenceRoom);
        return conferenceRoomEntityTransformer.toEntity(conferenceRoom);
    }

    public ConferenceRoomEntity update(ConferenceRoomEntity updatedConferenceRoomEntity) {
        ConferenceRoom updatedConferenceRoom = conferenceRoomEntityTransformer.fromEntity(updatedConferenceRoomEntity);
        ConferenceRoom conferenceRoom = crRepository.findById(updatedConferenceRoom.getId())
                .orElseThrow(() -> {
                    throw new NoSuchElementException();
                });
        if (updatedConferenceRoom.getName() != null) {
            conferenceRoom.setName(updatedConferenceRoom.getName());
        }
        if (updatedConferenceRoom.getIdentifier() != null) {
            conferenceRoom.setId(updatedConferenceRoom.getId());
        }
        if (updatedConferenceRoom.getLevel() != null) {
            conferenceRoom.setLevel(updatedConferenceRoom.getLevel());
        }
        if (updatedConferenceRoom.getNumberOfSeats() != null) {
            conferenceRoom.setNumberOfSeats(updatedConferenceRoom.getNumberOfSeats());
        }
        if (updatedConferenceRoom.getOrganization() != null) {
            Organization organization = orgRepository.findById(updatedConferenceRoom.getOrganization().getName())
                    .orElseThrow(() -> {
                        throw new NoSuchElementException();
                    });
            conferenceRoom.setOrganization(organization);
        }
        crRepository.findByNameAndOrganization_Name(conferenceRoom.getName(),
                        conferenceRoom.getOrganization().getName())
                .filter(cr -> !cr.getId().equals(conferenceRoom.getId()))
                .ifPresent(cr -> {
                    throw new IllegalArgumentException();
                });
        return conferenceRoomEntityTransformer.toEntity(crRepository.save(conferenceRoom));
    }

}
