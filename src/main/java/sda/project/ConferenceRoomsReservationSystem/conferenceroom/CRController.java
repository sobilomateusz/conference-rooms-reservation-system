package sda.project.ConferenceRoomsReservationSystem.conferenceroom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/conference_room")
@RestController
public class CRController {
    private final CRService crService;

    @Autowired
    public CRController(CRService crService) {
        this.crService = crService;
    }

    @PostMapping
    public ConferenceRoomEntity addConferenceRoom(@Validated(AddValidationGroup.class) @RequestBody ConferenceRoomEntity conferenceRoomEntity) {
        return crService.addConferenceRoom(conferenceRoomEntity);
    }

    @GetMapping("/{id}")
    public ConferenceRoomEntity getById(@PathVariable String id) {
        return crService.getById(id);
    }

    @GetMapping
    public List<ConferenceRoomEntity> getAll() {
        return crService.getAll();
    }

    @DeleteMapping("/{id}")
    public ConferenceRoomEntity deleteConferenceRoom(@PathVariable String id) {
        return crService.deleteConferenceRoom(id);
    }

    @PutMapping
    public ConferenceRoomEntity update(@Validated(UpdateValidationGroup.class) @RequestBody ConferenceRoomEntity updatedConferenceRoom) {
        return crService.update(updatedConferenceRoom);
    }
}
