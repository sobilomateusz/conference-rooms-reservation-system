package sda.project.ConferenceRoomsReservationSystem.conferenceroom;

import javax.validation.constraints.*;
import java.util.Objects;

public class ConferenceRoomEntity {

    private String id;
    @NotNull(groups = {AddValidationGroup.class})
    @NotBlank(groups = {AddValidationGroup.class, UpdateValidationGroup.class})
    @Size(min = 1, groups = {AddValidationGroup.class, UpdateValidationGroup.class})
    private String name;
    @Size(min = 1, groups = {AddValidationGroup.class, UpdateValidationGroup.class})
    private String identifier;
    @Min(value = 0, groups = {AddValidationGroup.class, UpdateValidationGroup.class})
    @Max(value = 10, groups = {AddValidationGroup.class, UpdateValidationGroup.class})
    private Integer level;
    private Boolean availability;
    private ConferenceRoom.NumberOfSeats numberOfSeats;
    private String organizationName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean getAvailability() {
        return availability;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    public ConferenceRoom.NumberOfSeats getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(ConferenceRoom.NumberOfSeats numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConferenceRoomEntity that = (ConferenceRoomEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(identifier, that.identifier) && Objects.equals(level, that.level) && Objects.equals(availability, that.availability) && Objects.equals(numberOfSeats, that.numberOfSeats) && Objects.equals(organizationName, that.organizationName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, identifier, level, availability, numberOfSeats, organizationName);
    }
}
