package sda.project.ConferenceRoomsReservationSystem.conferenceroom;

public class AlreadyExistingConferenceRoomException extends RuntimeException {
    public AlreadyExistingConferenceRoomException(String name) {
        super("Conference room with the name: " + name + " already exists in database!");
    }
}
