package sda.project.ConferenceRoomsReservationSystem.conferenceroom;

import org.springframework.stereotype.Component;
import sda.project.ConferenceRoomsReservationSystem.organization.Organization;

@Component
public class ConferenceRoomEntityTransformer {

    ConferenceRoomEntity toEntity(ConferenceRoom conferenceRoom) {
        ConferenceRoomEntity conferenceRoomEntity = new ConferenceRoomEntity();
        conferenceRoomEntity.setId(conferenceRoom.getId());
        conferenceRoomEntity.setName(conferenceRoom.getName());
        conferenceRoomEntity.setLevel(conferenceRoom.getLevel());
        conferenceRoomEntity.setIdentifier(conferenceRoom.getIdentifier());
        conferenceRoomEntity.setNumberOfSeats(conferenceRoom.getNumberOfSeats());
        conferenceRoomEntity.setOrganizationName(conferenceRoom.getOrganization().getName());
        conferenceRoomEntity.setAvailability(conferenceRoom.isAvailability());
        return conferenceRoomEntity;
    }

    ConferenceRoom fromEntity(ConferenceRoomEntity conferenceRoomEntity) {
        ConferenceRoom conferenceRoom = new ConferenceRoom();
        conferenceRoom.setId(conferenceRoomEntity.getId());
        conferenceRoom.setAvailability(conferenceRoomEntity.getAvailability());
        conferenceRoom.setLevel(conferenceRoomEntity.getLevel());
        conferenceRoom.setNumberOfSeats(conferenceRoomEntity.getNumberOfSeats());
        conferenceRoom.setName(conferenceRoomEntity.getName());
        conferenceRoom.setIdentifier(conferenceRoomEntity.getIdentifier());
        Organization organization = new Organization();
        organization.setName(conferenceRoomEntity.getOrganizationName());
        conferenceRoom.setOrganization(organization);
        return conferenceRoom;
    }
}
