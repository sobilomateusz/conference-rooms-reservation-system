package sda.project.ConferenceRoomsReservationSystem.conferenceroom;

import org.hibernate.annotations.GenericGenerator;
import sda.project.ConferenceRoomsReservationSystem.organization.Organization;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class ConferenceRoom {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String name;
    private String identifier;
    private Integer level;
    private Boolean availability;
    @Embedded
    private NumberOfSeats numberOfSeats;
    @ManyToOne
    private Organization organization;

    public ConferenceRoom() {
    }

    public ConferenceRoom(String name, String identifier, Integer level, Boolean availability, NumberOfSeats numberOfSeats, Organization organization) {
        this.name = name;
        this.identifier = identifier;
        this.level = level;
        this.availability = availability;
        this.numberOfSeats = numberOfSeats;
        this.organization = organization;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean isAvailability() {
        return availability;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    public NumberOfSeats getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(NumberOfSeats numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConferenceRoom that = (ConferenceRoom) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(identifier, that.identifier) && Objects.equals(level, that.level) && Objects.equals(availability, that.availability) && Objects.equals(numberOfSeats, that.numberOfSeats) && Objects.equals(organization, that.organization);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, identifier, level, availability, numberOfSeats, organization);
    }

    @Embeddable
    public static class NumberOfSeats {
        private int standing;
        private int sitting;
        private int laying;
        private int hanging;

        public NumberOfSeats() {
        }

        public NumberOfSeats(int standing, int sitting, int laying, int hanging) {
            this.standing = standing;
            this.sitting = sitting;
            this.laying = laying;
            this.hanging = hanging;
        }

        public int getStanding() {
            return standing;
        }

        public void setStanding(int standing) {
            this.standing = standing;
        }

        public int getSitting() {
            return sitting;
        }

        public void setSitting(int sitting) {
            this.sitting = sitting;
        }

        public int getLaying() {
            return laying;
        }

        public void setLaying(int laying) {
            this.laying = laying;
        }

        public int getHanging() {
            return hanging;
        }

        public void setHanging(int hanging) {
            this.hanging = hanging;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            NumberOfSeats that = (NumberOfSeats) o;
            return standing == that.standing && sitting == that.sitting && laying == that.laying && hanging == that.hanging;
        }

        @Override
        public int hashCode() {
            return Objects.hash(standing, sitting, laying, hanging);
        }
    }
}

