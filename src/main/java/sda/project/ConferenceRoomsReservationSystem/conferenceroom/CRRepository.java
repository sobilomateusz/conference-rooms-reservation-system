package sda.project.ConferenceRoomsReservationSystem.conferenceroom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CRRepository extends JpaRepository<ConferenceRoom, String> {
    Optional<ConferenceRoom> findByNameAndOrganization_Name(String name, String organizationName);
}
