package sda.project.ConferenceRoomsReservationSystem.reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;

@RequestMapping("/reservations")
@RestController
public class ReservationController {

    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping
    List<ReservationEntity> getAll(@RequestParam(required = false)
                                           String conferenceRoomName,
                                   @RequestParam(required = false)
                                           String conferenceRoomId,
                                   @RequestParam(required = false)
                                   @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
                                           LocalDateTime startDate,
                                   @RequestParam(required = false)
                                   @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
                                           LocalDateTime endDate) {
        return reservationService.getAll(conferenceRoomName, conferenceRoomId, startDate, endDate);
    }

    @GetMapping("/{id}")
    ReservationEntity getById(@PathVariable String id) {
        return reservationService.getById(id);
    }

    @PostMapping
    ReservationEntity add(@RequestBody ReservationEntity reservation) {
        return reservationService.add(reservation);
    }

    @PutMapping
    ReservationEntity update(@RequestBody ReservationEntity reservation) {
        return reservationService.update(reservation);
    }

    @DeleteMapping("/{id}")
    ReservationEntity delete(@PathVariable String id) {
        return reservationService.delete(id);
    }

    @ExceptionHandler(value = NoSuchElementException.class)
    public ResponseEntity<Object> handleException(NoSuchElementException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity<Object> handleException(IllegalArgumentException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
