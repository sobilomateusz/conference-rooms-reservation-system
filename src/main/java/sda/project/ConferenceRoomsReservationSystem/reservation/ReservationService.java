package sda.project.ConferenceRoomsReservationSystem.reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.project.ConferenceRoomsReservationSystem.conferenceroom.CRRepository;
import sda.project.ConferenceRoomsReservationSystem.conferenceroom.ConferenceRoom;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
class ReservationService {

    private final CRRepository conferenceRoomRepository;
    private final ReservationRepository reservationRepository;
    private final ReservationEntityTransformer reservationEntityTransformer;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository,
                              CRRepository conferenceRoomRepository,
                              ReservationEntityTransformer reservationEntityTransformer) {
        this.reservationRepository = reservationRepository;
        this.conferenceRoomRepository = conferenceRoomRepository;
        this.reservationEntityTransformer = reservationEntityTransformer;
    }

    List<ReservationEntity> getAll(String conferenceRoomName, String conferenceRoomId, LocalDateTime startDate, LocalDateTime endDate) {
        return mapToEntityList(getReservations(conferenceRoomName, conferenceRoomId, startDate, endDate));
    }


    private List<Reservation> getReservations(String conferenceRoomName, String conferenceRoomId, LocalDateTime startDate, LocalDateTime endDate) {
        if (conferenceRoomName != null) {
            return reservationRepository.findByConferenceRoom_Name(conferenceRoomName);
        } else if (conferenceRoomId != null) {
            return reservationRepository.findByConferenceRoom_Id(conferenceRoomId);
        } else if (startDate != null && endDate != null) {
            return reservationRepository
                    .findByStartDateLessThanEqualAndEndDateGreaterThanEqual(endDate, startDate);
        }
        return reservationRepository.findAll();
    }

    private List<ReservationEntity> mapToEntityList(List<Reservation> reservationList) {
        return reservationList
                .stream()
                .map(reservation -> reservationEntityTransformer.toEntity(reservation))
                .collect(Collectors.toList());
    }

    ReservationEntity add(ReservationEntity reservationEntity) {
        Reservation reservation = reservationEntityTransformer.fromEntity(reservationEntity);
        reservationRepository.findByConferenceRoom_IdAndStartDateLessThanEqualAndEndDateGreaterThanEqual(
                reservation.getConferenceRoom().getId(),
                reservation.getEndDate(),
                reservation.getStartDate()
        ).ifPresent(r -> {
            throw new IllegalArgumentException();
        });
        ConferenceRoom conferenceRoom = conferenceRoomRepository.findById(reservation.getConferenceRoom().getId())
                .orElseThrow(() -> {
                    throw new NoSuchElementException();
                });
        reservation.setConferenceRoom(conferenceRoom);
        return reservationEntityTransformer.toEntity(reservationRepository.save(reservation));
    }

    ReservationEntity update(ReservationEntity updatedReservationEntity) {
        boolean shouldCheckUniqueness = false;
        Reservation updatedReservation = reservationEntityTransformer.fromEntity(updatedReservationEntity);
        Reservation reservation = reservationRepository.findById(updatedReservation.getId()).orElseThrow(() -> {
            throw new NoSuchElementException();
        });
        if (updatedReservation.getConferenceRoom() != null) {
            ConferenceRoom conferenceRoom = conferenceRoomRepository.findById(updatedReservation.getId())
                    .orElseThrow(() -> {
                        throw new NoSuchElementException();
                    });
            reservation.setConferenceRoom(conferenceRoom);
            shouldCheckUniqueness = true;
        }
        if (updatedReservation.getName() != null) {
            reservation.setName(updatedReservation.getName());
        }
        if (updatedReservation.getStartDate() != null) {
            reservation.setStartDate(updatedReservation.getStartDate());
            shouldCheckUniqueness = true;
        }
        if (updatedReservation.getEndDate() != null) {
            reservation.setEndDate(updatedReservation.getEndDate());
            shouldCheckUniqueness = true;
        }
        if (shouldCheckUniqueness) {
            reservationRepository.findByConferenceRoom_IdAndStartDateLessThanEqualAndEndDateGreaterThanEqual(
                    reservation.getConferenceRoom().getId(),
                    reservation.getEndDate(),
                    reservation.getStartDate()
            ).ifPresent(r -> {
                throw new IllegalArgumentException();
            });
        }
        return reservationEntityTransformer.toEntity(reservationRepository.save(reservation));
    }

    ReservationEntity delete(String id) {
        Reservation reservation = reservationRepository.findById(id)
                .orElseThrow(() -> {
                    throw new NoSuchElementException();
                });
        reservationRepository.delete(reservation);
        return reservationEntityTransformer.toEntity(reservation);
    }

    ReservationEntity getById(String id) {
        Reservation reservation = reservationRepository.findById(id)
                .orElseThrow(() -> {
                    throw new NoSuchElementException();
                });
        return reservationEntityTransformer.toEntity(reservation);
    }
}
