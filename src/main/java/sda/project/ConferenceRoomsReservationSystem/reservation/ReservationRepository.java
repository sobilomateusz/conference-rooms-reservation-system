package sda.project.ConferenceRoomsReservationSystem.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, String> {
    Optional<Reservation> findByConferenceRoom_IdAndStartDateLessThanEqualAndEndDateGreaterThanEqual(String id, LocalDateTime endDate, LocalDateTime startDate);

    List<Reservation> findByConferenceRoom_Name(String name);

    List<Reservation> findByConferenceRoom_Id(String id);

    List<Reservation> findByStartDateLessThanEqualAndEndDateGreaterThanEqual(LocalDateTime endDate, LocalDateTime startDate);
}
