package sda.project.ConferenceRoomsReservationSystem.reservation;

import org.springframework.stereotype.Component;
import sda.project.ConferenceRoomsReservationSystem.conferenceroom.ConferenceRoom;

@Component
class ReservationEntityTransformer {

    ReservationEntity toEntity(Reservation reservation) {
        ReservationEntity reservationEntity = new ReservationEntity();
        reservationEntity.setName(reservation.getName());
        reservationEntity.setId(reservation.getId());
        reservationEntity.setStartDate(reservation.getStartDate());
        reservationEntity.setEndDate(reservation.getEndDate());
        reservationEntity.setConferenceRoomId(reservation.getConferenceRoom().getId());
        return reservationEntity;
    }

    Reservation fromEntity(ReservationEntity reservationEntity) {
        Reservation reservation = new Reservation();
        reservation.setId(reservationEntity.getId());
        reservation.setStartDate(reservationEntity.getStartDate());
        reservation.setEndDate(reservationEntity.getEndDate());
        reservation.setName(reservationEntity.getName());
        ConferenceRoom conferenceRoom = new ConferenceRoom();
        conferenceRoom.setId(reservationEntity.getConferenceRoomId());
        reservation.setConferenceRoom(conferenceRoom);
        return reservation;
    }
}


