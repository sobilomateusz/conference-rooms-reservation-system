package sda.project.ConferenceRoomsReservationSystem.reservation;

import org.hibernate.annotations.GenericGenerator;
import sda.project.ConferenceRoomsReservationSystem.conferenceroom.ConferenceRoom;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Reservation {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String name;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime startDate;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime endDate;
    @ManyToOne
    private ConferenceRoom conferenceRoom;

    public Reservation() {
    }

    public Reservation(String name, LocalDateTime startDate, LocalDateTime endDate, ConferenceRoom conferenceRoom) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.conferenceRoom = conferenceRoom;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endData) {
        this.endDate = endData;
    }

    public ConferenceRoom getConferenceRoom() {
        return conferenceRoom;
    }

    public void setConferenceRoom(ConferenceRoom conferenceRoom) {
        this.conferenceRoom = conferenceRoom;
    }

}
