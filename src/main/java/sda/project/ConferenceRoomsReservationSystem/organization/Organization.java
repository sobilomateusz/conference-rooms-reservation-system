package sda.project.ConferenceRoomsReservationSystem.organization;

import sda.project.ConferenceRoomsReservationSystem.conferenceroom.ConferenceRoom;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Organization {
    @Id
    private String name;
    private Long phoneNumber;
    @OneToMany(mappedBy = "organization")
    private List<ConferenceRoom> conferenceRooms = new ArrayList<>();

    public Organization(String name, Long phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public Organization() {
    }

    public List<ConferenceRoom> getConferenceRooms() {
        return conferenceRooms;
    }

    public void setConferenceRooms(List<ConferenceRoom> conferenceRooms) {
        this.conferenceRooms = conferenceRooms;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Organization that = (Organization) o;
        return Objects.equals(name, that.name) && Objects.equals(phoneNumber, that.phoneNumber) && Objects.equals(conferenceRooms, that.conferenceRooms);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, phoneNumber, conferenceRooms);
    }
}
