package sda.project.ConferenceRoomsReservationSystem.organization;

import org.springframework.stereotype.Component;
import sda.project.ConferenceRoomsReservationSystem.conferenceroom.ConferenceRoom;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrganizationEntityTransformer {
    public OrganizationEntity toEntity(Organization organization) {
        OrganizationEntity organizationEntity = new OrganizationEntity();
        organizationEntity.setName(organization.getName());
        organizationEntity.setPhoneNumber(organization.getPhoneNumber());
        List<OrganizationConferenceRoomEntity> conferenceRoomEntities = organization.getConferenceRooms()
                .stream().map(conferenceRoom -> {
                    OrganizationConferenceRoomEntity conferenceRoomEntity = new OrganizationConferenceRoomEntity();
                    conferenceRoomEntity.setId(conferenceRoom.getId());
                    conferenceRoomEntity.setName(conferenceRoom.getName());
                    conferenceRoomEntity.setIdentifier(conferenceRoom.getIdentifier());
                    return conferenceRoomEntity;
                }).collect(Collectors.toList());
        organizationEntity.setConferenceRooms(conferenceRoomEntities);
        return organizationEntity;
    }

    public Organization fromEntity(OrganizationEntity organizationEntity) {
        Organization organization = new Organization();
        organization.setName(organizationEntity.getName());
        organization.setPhoneNumber(organizationEntity.getPhoneNumber());
        List<ConferenceRoom> conferenceRooms = organizationEntity.getConferenceRooms()
                .stream().map(conferenceRoomEntity -> {
                    ConferenceRoom conferenceRoom = new ConferenceRoom();
                    conferenceRoom.setId(conferenceRoomEntity.getId());
                    conferenceRoom.setName(conferenceRoomEntity.getName());
                    conferenceRoom.setIdentifier(conferenceRoomEntity.getIdentifier());
                    return conferenceRoom;
                }).collect(Collectors.toList());
        organization.setConferenceRooms(conferenceRooms);
        return organization;
    }
}
