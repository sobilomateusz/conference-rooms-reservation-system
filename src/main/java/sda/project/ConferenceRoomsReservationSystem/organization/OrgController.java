package sda.project.ConferenceRoomsReservationSystem.organization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RequestMapping("/organization")
@RestController
public class OrgController {

    private final OrgService orgService;

    @Autowired
    public OrgController(OrgService orgService) {
        this.orgService = orgService;
    }

    @PostMapping
    public OrganizationEntity addOrganization(@Valid @RequestBody OrganizationEntity organizationEntity) {
        return orgService.addOrganization(organizationEntity);
    }

    @GetMapping("/{name}")
    public OrganizationEntity getById(@PathVariable String name) {
        return orgService.getById(name);
    }

    @GetMapping
    public List<OrganizationEntity> getAll(@RequestParam(required = false) String direction) {
        return orgService.getAll(direction);
    }

    @DeleteMapping("/{orgName}")
    public OrganizationEntity deleteOrganization(@PathVariable String orgName) {
        return orgService.deleteOrganization(orgName);
    }

    @PutMapping
    public OrganizationEntity update(@RequestBody OrganizationEntity updatedOrganizationEntity) {
        return orgService.update(updatedOrganizationEntity);
    }
}
