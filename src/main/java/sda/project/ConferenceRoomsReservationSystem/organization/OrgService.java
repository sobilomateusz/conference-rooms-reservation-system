package sda.project.ConferenceRoomsReservationSystem.organization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class OrgService {

    private final OrgRepository orgRepository;
    private final OrganizationEntityTransformer organizationEntityTransformer;

    @Autowired
    public OrgService(OrgRepository orgRepository, OrganizationEntityTransformer organizationEntityTransformer) {
        this.orgRepository = orgRepository;
        this.organizationEntityTransformer = organizationEntityTransformer;
    }

    public OrganizationEntity addOrganization(OrganizationEntity organizationEntity) {
        Organization organization = organizationEntityTransformer.fromEntity(organizationEntity);
        orgRepository.findById(organization.getName()).ifPresent(o -> {
            throw new AlreadyExistingOrganisationException(organization.getName());
        });
        return organizationEntityTransformer.toEntity(orgRepository.save(organization));
    }

    public List<OrganizationEntity> getAll(String direction) {
        Sort sort;
        if ("asc".equals(direction)) {
            sort = Sort.by(Sort.Direction.ASC, "name");
        } else if ("desc".equals(direction)) {
            sort = Sort.by(Sort.Direction.DESC, "name");
        } else {
            sort = Sort.unsorted();
        }
        return orgRepository.findAll(sort).stream()
                .map(organizationEntityTransformer::toEntity)
                .collect(Collectors.toList());
    }

    public OrganizationEntity getById(String name) {
        return orgRepository.findById(name)
                .map(organizationEntityTransformer::toEntity)
                .orElseThrow(() -> {
                    throw new NoSuchElementException();
                });
    }

    public OrganizationEntity deleteOrganization(String orgName) {
        Organization organization = orgRepository.findById(orgName)
                .orElseThrow(() -> {
                    throw new NoSuchElementException();
                });
        orgRepository.delete(organization);
        return organizationEntityTransformer.toEntity(organization);
    }

    public OrganizationEntity update(OrganizationEntity updatedOrganizationEntity) {
        Organization updatedOrganization = organizationEntityTransformer.fromEntity(updatedOrganizationEntity);
        Organization organization = orgRepository.findById(updatedOrganization.getName())
                .orElseThrow(() -> {
                    throw new NoSuchElementException();
                });
        organization.setPhoneNumber(updatedOrganization.getPhoneNumber());
        return organizationEntityTransformer.toEntity(orgRepository.save(organization));
    }
}
