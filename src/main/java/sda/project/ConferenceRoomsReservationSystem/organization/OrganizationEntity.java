package sda.project.ConferenceRoomsReservationSystem.organization;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OrganizationEntity {
    @Size(min = 2, max = 20)
    @NotBlank
    private String name;
    @Min(7)
    @NotNull
    private Long phoneNumber;
    private List<OrganizationConferenceRoomEntity> conferenceRooms = new ArrayList<>();

    public OrganizationEntity(String name, Long phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public OrganizationEntity() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<OrganizationConferenceRoomEntity> getConferenceRooms() {
        return conferenceRooms;
    }

    public void setConferenceRooms(List<OrganizationConferenceRoomEntity> conferenceRooms) {
        this.conferenceRooms = conferenceRooms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrganizationEntity that = (OrganizationEntity) o;
        return Objects.equals(name, that.name) && Objects.equals(phoneNumber, that.phoneNumber) && Objects.equals(conferenceRooms, that.conferenceRooms);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, phoneNumber, conferenceRooms);
    }
}
