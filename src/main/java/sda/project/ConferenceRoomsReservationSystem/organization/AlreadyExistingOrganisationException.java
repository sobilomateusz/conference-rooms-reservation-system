package sda.project.ConferenceRoomsReservationSystem.organization;

public class AlreadyExistingOrganisationException extends RuntimeException {
    public AlreadyExistingOrganisationException(String name) {
        super("Organization with the name: " + name + " already exists in database!");
    }
}
