package sda.project.ConferenceRoomsReservationSystem;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import sda.project.ConferenceRoomsReservationSystem.conferenceroom.AlreadyExistingConferenceRoomException;
import sda.project.ConferenceRoomsReservationSystem.organization.AlreadyExistingOrganisationException;

import java.util.NoSuchElementException;

@ControllerAdvice
public class ErrorHandler {
    @ExceptionHandler(value = AlreadyExistingOrganisationException.class)
    public ResponseEntity<Object> handleAlreadyExistingOrganisationException(AlreadyExistingOrganisationException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = AlreadyExistingConferenceRoomException.class)
    public ResponseEntity<Object> handleAlreadyExistingConferenceRoomException(AlreadyExistingConferenceRoomException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NoSuchElementException.class)
    public ResponseEntity<Object> handleException(NoSuchElementException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }
}
