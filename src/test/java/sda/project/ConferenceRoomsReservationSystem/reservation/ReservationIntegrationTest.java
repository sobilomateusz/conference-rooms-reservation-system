package sda.project.ConferenceRoomsReservationSystem.reservation;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import sda.project.ConferenceRoomsReservationSystem.ConferenceRoomsReservationSystemApplication;
import sda.project.ConferenceRoomsReservationSystem.conferenceroom.CRRepository;
import sda.project.ConferenceRoomsReservationSystem.conferenceroom.ConferenceRoom;
import sda.project.ConferenceRoomsReservationSystem.organization.OrgRepository;
import sda.project.ConferenceRoomsReservationSystem.organization.Organization;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = ConferenceRoomsReservationSystemApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class ReservationIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    OrgRepository organizationRepository;

    @Autowired
    CRRepository conferenceRoomRepository;

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    ObjectMapper objectMapper;

    @BeforeEach
    void tearDown() {
        organizationRepository.flush();
        conferenceRoomRepository.flush();
        reservationRepository.flush();
    }

    @Test
    void when_send_post_request_to_add_unique_reservation_then_reservation_should_be_added_to_db() throws Exception {
        //given
        Organization organization = new Organization("Intive", 123456789L);
        organizationRepository.save(organization);

        ConferenceRoom conferenceRoom = new ConferenceRoom("RED", "4.2", 4, true, new ConferenceRoom.NumberOfSeats(12,1,12,2), organization);
        ConferenceRoom conferenceRoomFromDb = conferenceRoomRepository.save(conferenceRoom);

        //when

        String addReservationResponse = mockMvc.perform(post("/reservations").contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"conferenceRoomId\": \"" + conferenceRoomFromDb.getId() + "\",\n" +
                        "  \"endDate\": \"22/08/2021 10:00\",\n" +
                        "  \"name\": \"R1\",\n" +
                        "  \"startDate\": \"22/08/2021 09:00\"\n" +
                        "}")).andReturn().getResponse().getContentAsString();
        //then
        ReservationEntity reservationEntity = objectMapper.readValue(addReservationResponse, ReservationEntity.class);
        Assertions.assertNotNull(reservationRepository.findById(reservationEntity.getId()).get());
    }

    @Test
    void when_send_post_request_to_add_non_unique_reservation_then_reservation_should_not_be_added() throws Exception {
        //given
        Organization organization = new Organization("Intive", 123456789L);
        organizationRepository.save(organization);

        ConferenceRoom conferenceRoom = new ConferenceRoom("RED", "4.2", 4, true, new ConferenceRoom.NumberOfSeats(12,13,2,2), organization);
        ConferenceRoom conferenceRoomFromDb = conferenceRoomRepository.save(conferenceRoom);

        Reservation reservation = new Reservation("R1", LocalDateTime.of(
                LocalDate.of(2021, 8, 22),
                LocalTime.of(9, 0)
        ),
                LocalDateTime.of(
                        LocalDate.of(2021, 8, 22),
                        LocalTime.of(10, 0)
                ), conferenceRoom);
        reservationRepository.save(reservation);

        //when
        //then
        mockMvc.perform(post("/reservations").contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"conferenceRoomId\": \"" + conferenceRoomFromDb.getId() + "\",\n" +
                        "  \"endDate\": \"22/08/2021 10:00\",\n" +
                        "  \"name\": \"R1\",\n" +
                        "  \"startDate\": \"22/08/2021 09:00\"\n" +
                        "}")).andExpect(status().isBadRequest());
    }

    @Test
    void when_send_post_request_to_add_reservation_that_partially_is_not_unique_then_reservation_should_not_be_added() throws Exception {
        //given
        Organization organization = new Organization("Intive", 123456789L);
        organizationRepository.save(organization);

        ConferenceRoom conferenceRoom = new ConferenceRoom("RED", "4.2", 4, true, new ConferenceRoom.NumberOfSeats(12,12,12,12), organization);
        ConferenceRoom conferenceRoomFromDb = conferenceRoomRepository.save(conferenceRoom);

        Reservation reservation = new Reservation("R1", LocalDateTime.of(
                LocalDate.of(2021, 8, 22),
                LocalTime.of(9, 0)
        ),
                LocalDateTime.of(
                        LocalDate.of(2021, 8, 22),
                        LocalTime.of(10, 0)
                ), conferenceRoom);
        reservationRepository.save(reservation);

        //when
        //then
        mockMvc.perform(post("/reservations").contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"conferenceRoomId\": \"" + conferenceRoomFromDb.getId() + "\",\n" +
                        "  \"endDate\": \"22/08/2021 10:30\",\n" +
                        "  \"name\": \"R1\",\n" +
                        "  \"startDate\": \"22/08/2021 09:30\"\n" +
                        "}")).andExpect(status().isBadRequest());
    }

    @Test
    void when_send_post_request_to_add_reservation_that_partially_is_not_unique_then_reservation_should_not_be_added_2() throws Exception {
        //given
        Organization organization = new Organization("Intive",123456789L );
        organizationRepository.save(organization);

        ConferenceRoom conferenceRoom = new ConferenceRoom("RED", "4.2", 4, true, new ConferenceRoom.NumberOfSeats(12,12,12,12), organization);
        ConferenceRoom conferenceRoomFromDb = conferenceRoomRepository.save(conferenceRoom);

        Reservation reservation = new Reservation("R1", LocalDateTime.of(
                LocalDate.of(2021, 8, 22),
                LocalTime.of(9, 0)
        ),
                LocalDateTime.of(
                        LocalDate.of(2021, 8, 22),
                        LocalTime.of(10, 0)
                ), conferenceRoom);
        reservationRepository.save(reservation);

        //when
        //then
        mockMvc.perform(post("/reservations").contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"conferenceRoomId\": \"" + conferenceRoomFromDb.getId() + "\",\n" +
                        "  \"endDate\": \"22/08/2021 09:30\",\n" +
                        "  \"name\": \"R1\",\n" +
                        "  \"startDate\": \"22/08/2021 08:45\"\n" +
                        "}")).andExpect(status().isBadRequest());
    }
}
