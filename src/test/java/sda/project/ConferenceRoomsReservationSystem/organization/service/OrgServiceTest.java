package sda.project.ConferenceRoomsReservationSystem.organization.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import sda.project.ConferenceRoomsReservationSystem.organization.*;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class OrgServiceTest {
    @TestConfiguration
    static class TestConfigurationServiceProvider {
        @Bean
        OrgService orgService(OrgRepository orgRepository) {
            return new OrgService(orgRepository, new OrganizationEntityTransformer());
        }
    }

    @Autowired
    private OrgService orgService;
    @MockBean
    private OrgRepository orgRepository;

    @Test
    void when_send_get_request_with_valid_id_then_organization_with_provided_id_should_be_returned() {
        //given
        Organization organization = new Organization("Nike", 235236534L);
        Mockito.when(orgRepository.findById("Nike")).thenReturn(Optional.of(organization));
        //when
        OrganizationEntity organizationReturned = orgService.getById("Nike");
        //then
        assertEquals("Nike", organizationReturned.getName());
        assertEquals(235236534L, organizationReturned.getPhoneNumber());
    }

    @Test
    void when_send_get_request_with_invalid_id_then_not_found_exception_should_returned() {
        //given
        Mockito.when(orgRepository.findById("Intive")).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(NoSuchElementException.class, () -> {
            orgService.getById("Intive");
        });
    }

    @Test
    void when_adding_non_existing_organisation_to_db_then_it_should_be_added() {
        //given
        Organization organization = new Organization("Nike", 34646456456L);
        Mockito.when(orgRepository.findById("Nike")).thenReturn(Optional.empty());
        Mockito.when(orgRepository.save(organization)).thenReturn(organization);
        OrganizationEntity organizationEntity = new OrganizationEntity();
        organizationEntity.setName("Nike");
        organizationEntity.setPhoneNumber(34646456456L);
        //when
        orgService.addOrganization(organizationEntity);
        //then
        Mockito.verify(orgRepository).save(organization);
    }

    @Test
    void when_adding_existing_organization_then_already_existing_organization_exception_should_be_thrown() {
        //given
        Organization organization = new Organization("Nike", 34646456456L);
        Mockito.when(orgRepository.findById("Nike")).thenReturn(Optional.of(organization));
        OrganizationEntity organizationEntity = new OrganizationEntity();
        organizationEntity.setName("Nike");
        organizationEntity.setPhoneNumber(34646456456L);
        //when
        //then
        assertThrows(AlreadyExistingOrganisationException.class, () -> {
            orgService.addOrganization(organizationEntity);
        }, "Organization with the name: " + organization.getName() + " already exists in database!");
    }

    @Test
    void when_send_delete_request_with_existing_organization_name_then_organization_should_be_removed() {
        //given
        Organization organization = new Organization("Intive", 23434535L);
        Mockito.when(orgRepository.findById("Intive")).thenReturn(Optional.of(organization));

        //when
        orgService.deleteOrganization("Intive");

        //then
        Mockito.verify(orgRepository).delete(organization);
    }

    @Test
    void when_send_delete_request_with_non_existing_organization_name_then_exception_should_be_thrown() {
        //given
        Mockito.when(orgRepository.findById("Intive")).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(NoSuchElementException.class, () -> {
            orgService.deleteOrganization("Intive");
        });
    }

    @Test
    void when_send_get_request_to_get_organization_list_in_asc_order_then_ordered_list_should_be_returned() {
        //given
        ArgumentCaptor<Sort> captor = ArgumentCaptor.forClass(Sort.class);

        //when
        orgService.getAll("asc");

        //then
        Mockito.verify(orgRepository).findAll(captor.capture());
        assertTrue(captor.getValue().getOrderFor("name").isAscending());
    }

    @Test
    void when_send_get_request_to_get_organization_list_in_dsc_order_then_ordered_list_should_be_returned() {
        //given
        ArgumentCaptor<Sort> captor = ArgumentCaptor.forClass(Sort.class);

        //when
        orgService.getAll("desc");

        //then
        Mockito.verify(orgRepository).findAll(captor.capture());
        assertTrue(captor.getValue().getOrderFor("name").isDescending());
    }

    @Test
    void when_send_get_request_to_get_unsorted_organization_list_then_unsorted_organization_list_should_be_returned() {
        //given
        ArgumentCaptor<Sort> captor = ArgumentCaptor.forClass(Sort.class);

        //when
        orgService.getAll("");

        //then
        Mockito.verify(orgRepository).findAll(captor.capture());
        assertTrue(captor.getValue().isUnsorted());
    }


}