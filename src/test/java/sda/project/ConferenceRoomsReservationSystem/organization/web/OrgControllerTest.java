package sda.project.ConferenceRoomsReservationSystem.organization.web;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import sda.project.ConferenceRoomsReservationSystem.organization.*;

import java.util.Arrays;
import java.util.NoSuchElementException;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(OrgController.class)
class OrgControllerTest {
    @MockBean
    private OrgService organizationService;

    @Autowired
    MockMvc mockMvc;

    @Test
    void when_send_get_request_by_name_with_existing_organization_name_then_organization_should_be_returned() throws Exception {
        //given
        OrganizationEntity organization = new OrganizationEntity();
        organization.setName("Nike");
        organization.setPhoneNumber(87587585L);
        Mockito.when(organizationService.getById("Nike")).thenReturn(organization);

        //when
        //then
        mockMvc.perform(get("/organization/Nike").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", equalTo("Nike")))
                .andExpect(jsonPath("$.phoneNumber", equalTo(87587585)));
    }

    @Test
    void when_send_get_request_by_name_which_is_not_exist_in_repo_then_not_found_error_should_be_returned() throws Exception {
        //given
        Mockito.when(organizationService.getById("Intive")).thenThrow(NoSuchElementException.class);

        //when
        //then
        mockMvc.perform(get("/organization/Intive").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void when_send_post_request_which_has_not_existing_organization_then_organization_should_be_returned_as_response() throws Exception {
        //given
        OrganizationEntity organization = new OrganizationEntity();
        organization.setName("Intive");
        organization.setPhoneNumber(825788257L);
        Mockito.when(organizationService.addOrganization(organization)).thenReturn(organization);

        //when
        //then
        mockMvc.perform(post("/organization/")
                        .content("{ \"phoneNumber\": 825788257 ," +
                                " \"name\": \"Intive\"}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", equalTo("Intive")))
                .andExpect(jsonPath("$.phoneNumber", equalTo(825788257)));
    }

    @Test
    void when_send_post_request_which_has_organization_then_error_should_be_returned() throws Exception {
        //given
        OrganizationEntity organization = new OrganizationEntity();
        organization.setName("Intive");
        organization.setPhoneNumber(825788257L);
        Mockito.when(organizationService.addOrganization(organization)).thenThrow(AlreadyExistingOrganisationException.class);

        //when
        //then
        mockMvc.perform(post("/organization/")
                        .content("{ \"phoneNumber\": 825788257," +
                                " \"name\": \"Intive\"}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void when_send_get_request_with_asc_order_then_ordered_list_should_be_returned() throws Exception {
        //given
        Mockito.when(organizationService.getAll("asc")).thenReturn(Arrays.asList(
                new OrganizationEntity("a", 75758855L),
                new OrganizationEntity("b", 87557859L),
                new OrganizationEntity("c", 8968768576L)
        ));

        //when
        //then
        mockMvc.perform(get("/organization?direction=asc").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name", equalTo("a")))
                .andExpect(jsonPath("$[1].name", equalTo("b")))
                .andExpect(jsonPath("$[2].name", equalTo("c")));
    }

}