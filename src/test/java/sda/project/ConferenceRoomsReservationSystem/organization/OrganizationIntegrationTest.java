package sda.project.ConferenceRoomsReservationSystem.organization;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import sda.project.ConferenceRoomsReservationSystem.ConferenceRoomsReservationSystemApplication;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = ConferenceRoomsReservationSystemApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class OrganizationIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    OrgRepository orgRepository;

    @Test
    void when_send_get_by_name_request_then_record_from_repo_should_be_returned() throws Exception {
        //given
        orgRepository.save(new Organization("Intive", 878585989L));

        //when
        //then
        mockMvc.perform(get("/organization/Intive").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", equalTo("Intive")))
                .andExpect(jsonPath("$.phoneNumber", equalTo(878585989)));
    }

    @Test
    void when_send_post_request_to_add_new_organization_then_organization_should_be_added() throws Exception {
        //given
        String jsonContent = "{ \"phoneNumber\": 878585989," +
                " \"name\": \"Intive\"}";

        //when
        mockMvc.perform(post("/organization/")
                .content(jsonContent).contentType(MediaType.APPLICATION_JSON));
        //then
        Organization organization = orgRepository.findById("Intive").get();
        Assertions.assertEquals("Intive", organization.getName());
        Assertions.assertEquals(878585989, organization.getPhoneNumber() );
    }
}
