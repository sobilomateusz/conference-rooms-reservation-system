package sda.project.ConferenceRoomsReservationSystem.organization.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import sda.project.ConferenceRoomsReservationSystem.organization.OrgRepository;
import sda.project.ConferenceRoomsReservationSystem.organization.Organization;

import javax.persistence.EntityManager;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class OrgRepositoryTest {
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private OrgRepository orgRepository;

    @Test
    void when_db_is_empty_then_using_find_all_on_repository_should_return_empty_result_list() {
        //given
        //when
        List<Organization> organizations = orgRepository.findAll();
        //then
        assertEquals(0, organizations.size());
    }

    @Test
    void when_db_has_two_records_then_using_find_all_should_return_list_of_two_elements() {
        //given
        entityManager.persist(new Organization("Nike", 7923457234L));
        entityManager.persist(new Organization("Adidas", 342352353L));
        //when
        List<Organization> organizations = orgRepository.findAll();
        //then
        assertEquals(2, organizations.size());
        assertEquals("Nike", organizations.get(0).getName());
        assertEquals("Adidas", organizations.get(1).getName());
    }

    @Test
    void when_db_has_3_elements_then_using_find_by_id_should_return_one_record_with_given_id() {
        //given
        entityManager.persist(new Organization("Nike", 7923457234L));
        entityManager.persist(new Organization("Adidas", 342352353L));
        entityManager.persist(new Organization("Puma", 453464534L));
        //when
        Organization organizationById = orgRepository.findById("Adidas").get();
        //then
        assertEquals("Adidas", organizationById.getName());
        assertEquals(342352353L, organizationById.getPhoneNumber());
    }


}